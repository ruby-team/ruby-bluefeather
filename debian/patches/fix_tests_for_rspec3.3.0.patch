From: Lucas Albuquerque Medeiros de Moura <lucas.moura128@gmail.com>
Date: Wed, 2 Mar 2016 14:06:40 -0300
Subject: Fix test for rspec version 3.3.0

In order for the tests to run on rspec 3.3.0, it was necessary
to update the rspec methods be_false and be_true to be_falsey and
be_truthy, respectivelly. Also, it was necessary to change the
shared_as blocks for shared_context ones, since it is the new way
for rspec tests.

 This fix will not be sent to the upstream, because the tool being used
 to hold the source code doesn't seem to allow merge requests.

Bug-Debian: https://bugs.debian.org/794161
Forwarded: not-needed
---
 spec/code-block.rb | 22 +++++++++++-----------
 spec/cui.rb        |  6 +++---
 spec/encoding.rb   | 28 ++++++++++++++--------------
 spec/table.rb      | 22 +++++++++++-----------
 spec/toc.rb        | 10 +++++-----
 5 files changed, 44 insertions(+), 44 deletions(-)

diff --git a/spec/code-block.rb b/spec/code-block.rb
index 488ed8b..cef4a60 100644
--- a/spec/code-block.rb
+++ b/spec/code-block.rb
@@ -30,19 +30,19 @@ describe 'Fenced Code Block:' do
 
 
 	describe 'Basic:' do
-		share_as :Examples do
-			specify 'overview' do
+		shared_context 'examples' do
+			it 'overview' do
 				@doc.should have_elements(1, 'pre code')
 				@doc.should_not have_element('p')
 			end
 			
-			specify 'body' do
+			it 'body' do
 				@doc.at('pre code').inner_text.should == "Fenced Code\nBlock\n"
 			end
 		end
 		
-		share_as :NegativeExamples do
-			specify 'overview' do
+		shared_context 'negative examples' do
+			it 'overview' do
 				@doc.should_not have_elements(1, 'pre code')
 				@doc.should have_elements(1, 'p')
 			end
@@ -50,7 +50,7 @@ describe 'Fenced Code Block:' do
 
 	
 		describe 'Standard:' do
-			include Examples
+			include_context "examples"
 			before(:all) do
 				@src = "~~~\nFenced Code\nBlock\n~~~"
 			end
@@ -59,28 +59,28 @@ describe 'Fenced Code Block:' do
 
 		
 		describe 'Many Symbols:' do
-			include Examples
+			include_context "examples"
 			before(:all) do
 				@src = "~~~~~~\nFenced Code\nBlock\n~~~~~~"
 			end
 		end
 		
 		describe 'Original Code Block:' do
-			include Examples
+			include_context "examples"
 			before(:all) do
 				@src = "    Fenced Code\n    Block"
 			end
 		end
 		
 		describe 'Bad (wrong number of symbols):' do
-			include NegativeExamples
+			include_context "negative examples"
 			before(:all) do
 				@src = "~~~~~~\nFenced Code\nBlock\n~~~"
 			end
 		end
 
 		describe 'Bad (symbols is not on line-head.):' do
-			include NegativeExamples
+			include_context "negative examples"
 			before(:all) do
 				@src = " ~~~\nFenced Code\nBlock\n ~~~"
 			end
@@ -250,4 +250,4 @@ MARKDOWN
 
 	end
 	
-end
\ No newline at end of file
+end
diff --git a/spec/cui.rb b/spec/cui.rb
index 98c32d1..dbc7fa2 100644
--- a/spec/cui.rb
+++ b/spec/cui.rb
@@ -57,18 +57,18 @@ describe 'CUI:' do
 		sample_src = (Pathname.new(__FILE__).parent + 'text/encoding_sample_ascii.bfdoc').to_s
 		valids.each do |name|
 			specify "'#{name}' is valid" do
-				@cui.run(['--encoding', name, '-']).should be_true
+				@cui.run(['--encoding', name, '-']).should be_truthy
 			end
 		end
 		
 		invalids = %w(euc_jp none jis)
 		invalids.each do |name|
 			specify "'#{name}' is invalid" do
-				@cui.run(['--encoding', name, '-']).should be_false
+				@cui.run(['--encoding', name, '-']).should be_falsey
 			end
 		end
 		
 	end
 
 
-end
\ No newline at end of file
+end
diff --git a/spec/encoding.rb b/spec/encoding.rb
index ab08add..d1b606c 100644
--- a/spec/encoding.rb
+++ b/spec/encoding.rb
@@ -8,9 +8,9 @@ require(Pathname.new(__FILE__).parent + 'lib/common.rb')
 describe 'Internal encoding specification:' do
 	specify "utf-8 bom check" do
 		src = '日本語'.kconv(Kconv::SJIS, Kconv::UTF8)
-		BlueFeather::Util.utf8_bom?(src).should be_false
+		BlueFeather::Util.utf8_bom?(src).should be_falsey
 		bom_src = "\xef\xbb\xbf日本語"
-		BlueFeather::Util.utf8_bom?(bom_src).should be_true
+		BlueFeather::Util.utf8_bom?(bom_src).should be_truthy
 	end
 	
 	specify "'shift-jis' is available:" do
@@ -27,26 +27,26 @@ describe 'Encoding of files:' do
 		@doc = Nokogiri(@html)
 	end
 	
-	share_as :Base do
-		specify 'header parse - css' do
+	shared_context 'base' do
+		it 'header parse - css' do
 			@doc.should have_element('link[@rel="stylesheet"][@href="style.css"]')
 		end
 
-		specify 'header parse - encoding' do
+		it 'header parse - encoding' do
 			@doc.should have_element(%Q|meta[@http-equiv="Content-Type"][@content="text/html; charset=#{@charset}"]|)
 		end
 		
-		specify 'header parse - title' do
+		it 'header parse - title' do
 			base = '日本語の題名'.kconv(@kconv_encoding, Kconv::UTF8)
 			@doc.at('title').inner_html.should == base
 		end
 		
-		specify 'content parse - p' do
+		it 'content parse - p' do
 			base = 'このテキストファイルは、エンコーディングテスト用のテキストファイルです。'.kconv(@kconv_encoding, Kconv::UTF8)
 			@doc.at('p').inner_html.should == base
 		end
 		
-		specify 'content parse - ul' do
+		it 'content parse - ul' do
 			@doc.should have_elements(1, 'ul')
 			
 			items = @doc.search('ul li')
@@ -60,7 +60,7 @@ describe 'Encoding of files:' do
 	
 	
 	describe 'Default:' do
-		include Base
+		include_context 'base'
 		
 		before(:all) do
 			@src_name = 'encoding_sample_default.bfdoc'
@@ -71,7 +71,7 @@ describe 'Encoding of files:' do
 
 
 	describe 'UTF-8 (BOM):' do
-		include Base
+		include_context 'base'
 		
 		before(:all) do
 			@src_name = 'encoding_sample_utf-8.bfdoc'
@@ -82,7 +82,7 @@ describe 'Encoding of files:' do
 	
 	
 	describe 'UTF-8N:' do
-		include Base
+		include_context 'base'
 		
 		before(:all) do
 			@src_name = 'encoding_sample_utf-8n.bfdoc'
@@ -93,7 +93,7 @@ describe 'Encoding of files:' do
 
 	
 	describe 'Shift_JIS:' do
-		include Base
+		include_context 'base'
 		
 		before(:all) do
 			@src_name = 'encoding_sample_shift-jis.bfdoc'
@@ -103,7 +103,7 @@ describe 'Encoding of files:' do
 	end
 	
 	describe 'EUC-JP:' do
-		include Base
+		include_context 'base'
 		
 		before(:all) do
 			@src_name = 'encoding_sample_euc-jp.bfdoc'
@@ -111,4 +111,4 @@ describe 'Encoding of files:' do
 			@kconv_encoding = Kconv::EUC
 		end
 	end
-end
\ No newline at end of file
+end
diff --git a/spec/table.rb b/spec/table.rb
index 0f52cd0..98800ce 100644
--- a/spec/table.rb
+++ b/spec/table.rb
@@ -10,12 +10,12 @@ describe 'Simple Table:' do
 		@doc = Nokogiri(@html)
 	end
 	
-	share_as :StandardTable do
-		specify "table overview" do
+	shared_context 'standard table' do
+		it "table overview" do
 			@doc.should have_elements(1, 'table')
 		end
 		
-		specify "size 3x3" do
+		it "size 3x3" do
 			@doc.should have_elements(3, 'tr')
 			@doc.should have_elements(1, 'thead tr')
 			@doc.should have_elements(2, 'tbody tr')
@@ -23,7 +23,7 @@ describe 'Simple Table:' do
 			@doc.should have_elements(6, 'tbody tr td')
 		end
 		
-		specify "alignment" do
+		it "alignment" do
 			@doc.search('tr').each do |tr|
 				cells = tr.search('th, td')
 				cells[0]['style'].should be_nil
@@ -32,14 +32,14 @@ describe 'Simple Table:' do
 			end
 		end
 		
-		specify "purity" do
+		it "purity" do
 			@doc.should_not have_element('p table')
 		end
 	end
 
 
 	describe 'no-doubleline' do
-		include StandardTable
+		include_context 'standard table'
 	
 		before(:all) do
 			@src = "|h1|h2|h3\n|-|-:|:-:\n|d11|d21|d31\n|d12|d22|d32"
@@ -49,7 +49,7 @@ describe 'Simple Table:' do
 	
 	
 	describe 'doubleline: ' do
-		include StandardTable
+		include_context 'standard table'
 
 		before(:all) do
 			@src = "|h1|h2|h3|\n|-|-:|:-:|\n|d11|d21|d31|\n|d12|d22|d32|"
@@ -57,7 +57,7 @@ describe 'Simple Table:' do
 	end
 	
 	describe 'inner space:' do
-		include StandardTable
+		include_context 'standard table'
 	
 		before(:all) do
 			@src = "| h1 | h2 | h3 \n| - | -: | :-: \n| d11 | d21 | d31\n| d12 | d22 | d32"
@@ -65,7 +65,7 @@ describe 'Simple Table:' do
 	end
 
 	describe 'left space:' do
-		include StandardTable
+		include_context 'standard table'
 	
 		before(:all) do
 			@src = " | h1 | h2 | h3 \n | - | -: | :-: \n | d11 | d21 | d31\n | d12 | d22 | d32"
@@ -74,7 +74,7 @@ describe 'Simple Table:' do
 
 
 	describe 'in definition list:' do
-		include StandardTable
+		include_context 'standard table'
 	
 		before(:all) do
 			@src = <<MARKDOWN
@@ -91,4 +91,4 @@ MARKDOWN
 	end
 
 
-end
\ No newline at end of file
+end
diff --git a/spec/toc.rb b/spec/toc.rb
index 2273bdf..ef21995 100644
--- a/spec/toc.rb
+++ b/spec/toc.rb
@@ -29,8 +29,8 @@ MARKDOWN
 
 	
 	
-		share_as :ContentOverview do
-			specify 'overview' do
+		shared_context 'content overview' do
+		    it 'overview' do
 				@doc.should have_element('ul')
 				@doc.should have_elements(1, 'h1')
 				@doc.should have_elements(3, 'h2')
@@ -40,7 +40,7 @@ MARKDOWN
 	
 	
 		describe 'basic:' do
-			include ContentOverview
+			 include_context 'content overview'
 			before(:all) do
 				@src = "{toc}\n\n" +  @header_part
 			end
@@ -73,7 +73,7 @@ MARKDOWN
 		
 		
 		describe 'range:' do
-			include ContentOverview
+			 include_context 'content overview'
 			before(:all) do
 				@src = "{toc:h1..h2}\n\n" +  @header_part
 			end
@@ -144,4 +144,4 @@ MARKDOWN
 	
 
 
-end
\ No newline at end of file
+end
