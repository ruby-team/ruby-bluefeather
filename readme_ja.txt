BlueFeather - Extended MarkDown Converter
====

BlueFeather は、拡張 Markdown 記法で書かれたテキストを html に変換するソフトウェアです。
コマンドラインツールと、Ruby スクリプト内で変換を行うためのライブラリがセットになっています。

詳しくは doc ディレクトリ内のマニュアル（doc/index.html）を参照してください。



インストール
----
ruby のインストールされたPCで、付属している setup.rb を実行してください。

	% ruby setup.rb



ライセンス
----
GPL（General Public License）のバージョン2、またはそれ以降のバージョンに従います。



配布サイト
----
<http://ruby.morphball.net/bluefeather>



連絡先
----
Dice <tetradice@gmail.com>