#!/usr/bin/ruby
#
# Unit test for bugs found in BlueFeather
# $Id: 10_Bug.tests.rb 68 2004-08-25 05:14:37Z ged $
#
# Copyright (c) 2004 The FaerieMUD Consortium, 2009 Dice.
#
# Changes: [2009-02-14] adapt to BlueFeather

if !defined?( BlueFeather ) || !defined?( BlueFeather::TestCase )
	require 'pathname'
	$LOAD_PATH << Pathname.new(__FILE__).dirname.expand_path.to_s
	require 'bftestcase'
end


require 'timeout'

### This test case tests ...
class BugsTestCase < BlueFeather::TestCase
	BaseDir = File::dirname( File::dirname(File::expand_path( __FILE__ )) )

	### :TODO: Add more documents and test their transforms.

	def test_10_regexp_engine_overflow_bug
		contents = File::read( File::join(BaseDir,"original-tests/data/re-overflow.txt") )

		assert_nothing_raised {
			BlueFeather.parse( contents )
		}
	end
	
	def test_15_regexp_engine_overflow_bug2
		contents = File::read( File::join(BaseDir,"original-tests/data/re-overflow2.txt") )

		assert_nothing_raised {
			BlueFeather.parse( contents )
		}
	end
	
end


__END__

