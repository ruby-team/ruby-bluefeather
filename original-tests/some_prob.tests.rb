#!/usr/bin/ruby
# additional test: since BlueFeather 0.32

if !defined?( BlueFeather ) || !defined?( BlueFeather::TestCase )
	require 'pathname'
	$LOAD_PATH << Pathname.new(__FILE__).dirname.expand_path.to_s
	require 'bftestcase'
end


### This test case tests ...
class SubfunctionsTestCase < BlueFeather::TestCase

  def test_termination_of_meta_tag
    result = BlueFeather.parse_document('').split("\n").
             grep(/^<meta/).first
    expected = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'
    assert_equal(expected, result)
  end
  
  def test_metadata_description
    bfdoc = 'Description: description text.'
    result = BlueFeather.parse_document(bfdoc).split("\n").
             grep(/^<meta name/).first
    expected = '<meta name="description" content="description text." />'
    assert_equal(expected, result)
  end
  
  def test_metadata_keyword
    bfdoc = 'Keywords: key1, key2'
    result = BlueFeather.parse_document(bfdoc).split("\n").
             grep(/^<meta name/).first
    expected = '<meta name="keywords" content="key1, key2" />'
    assert_equal(expected, result)
  end

  # IE で、title タグ内が日本語テキストの時、
  # MIMEタイプと文字コードセットの指定が title タグの前に無いと、
  # 初回ページを表示する際に何も表示されない。
  def test_order_of_charcode_spec_and_title
    bfdoc = '# title'
    result = BlueFeather.parse_document('# title').split("\n").
             grep(/^<title|^<meta/).map {|line| line[0, 5]}
    expected = %w(<meta <titl)
    assert_equal(expected, result)
  end

  def test_code_span_surrounded_by_double_backticks_at_begining_of_line
    result = BlueFeather.parse('``code span``')
    expected = '<p><code>code span</code></p>'
    assert_equal(expected, result)
  end

  def test_backslash_in_code_block
    result = BlueFeather.parse('    1986\. What a great season.' + "\n")
    expected = <<-'EOS'
<pre><code>1986\. What a great season.
</code></pre>
    EOS
    assert_equal(expected.chomp, result)
  end
	
  def test_header_tag_containing_colon_at_begining_of_doc
    bfdoc = '<h1>hdr:level1</h1>'
    result = BlueFeather.parse_document(bfdoc).split("\n").
             grep(/^<h1/).first
    expected = bfdoc
    assert_equal(expected, result)
  end

  def test_header_containing_colon_at_begining_of_doc
    bfdoc = '# markdown: syntax'
    result = BlueFeather.parse_document(bfdoc).split("\n").
             grep(/^<h1/).first.to_s[0,3]
    expected = '<h1'
    assert_equal(expected, result)
  end

end
