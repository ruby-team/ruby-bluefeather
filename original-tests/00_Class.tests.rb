#!/usr/bin/ruby
#
# Unit test for the BlueFeather class object 
# $Id: TEMPLATE.rb.tpl,v 1.2 2003/09/11 04:59:51 deveiant Exp $
#
# Copyright (c) 2004 The FaerieMUD Consortium, 2009 Dice.
#
# Changes: [2009-02-14] adapt to BlueFeather

if !defined?( BlueFeather ) || !defined?( BlueFeather::TestCase )
	require 'pathname'
	$LOAD_PATH << Pathname.new(__FILE__).dirname.expand_path.to_s
	require 'bftestcase'
end


### This test case tests ...
class BlueFeatherClassTestCase < BlueFeather::TestCase

	TestString = "foo"

	def test_01_instantiation
		printTestHeader "BlueFeather: Instantiation"
		rval = nil
		
		# With no argument... ("")
		assert_nothing_raised {
			rval = BlueFeather::Parser.new
		}

	end
end

