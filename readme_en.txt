BlueFeather - Extended MarkDown Converter
====

 BlueFeather is software for converting text written by extended Markdown like 
PHP Markdown Extra to html. It is pair of command-line tool and pure Ruby
library.

 BlueFeather is based on BlueCloth 1.0.0. And that, some extensions is added --
known bug fix, changing interface and extension of Markdown syntax.

 See doc/en/index.html, please.



Install
----
Run setup.rb on ruby-installed PC.

	% ruby setup.rb



License
----
GPL (General Public License) version 2 or later



Website
----
<http://ruby.morphball.net/bluefeather>



Contact
----
Dice <tetradice@gmail.com>