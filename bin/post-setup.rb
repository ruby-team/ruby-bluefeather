
#==============================================================================#
# $Id: post-setup.rb,v 1.1 2006/04/27 01:25:21 yuya Exp $
#==============================================================================#

if /mswin32/ =~ RUBY_PLATFORM
  File.open("bluefeather.bat", "w") { |file|
    file.puts(%|@echo off|)
    file.puts(%|"%~dp0ruby" -x "%~f0" %*|)
    file.puts(%|goto endofruby|)
		file.puts
    file.write(File.read("bluefeather"))
		file.puts
    file.puts(%|__END__|)
    file.puts(%|:endofruby|)
  }
end

#==============================================================================#
#==============================================================================#
