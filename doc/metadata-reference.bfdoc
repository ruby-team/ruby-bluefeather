Title: メタデータリファレンス - BlueFeather マニュアル
CSS: black.css

<div class="back"><a href="index.html">BlueFeather マニュアル</a></div>

メタデータリファレンス
====

文書ファイル（*.bfdoc）の頭に、`:` 記号で区切られたキーと値の組（ヘッダー）を書いておくことで、その文書にタイトルなどの情報（メタデータ）を付け加えることができます。

	Title: 文書名
	CSS: style.css
	Atom-Feed: info/atom.xml
	
	ここから本文

これらの文書メタデータは、`parse_document` や `parse_document_file` などのメソッドを使って解釈したときにのみ有効です。

キー名の大文字/小文字は区別されません。


{toc}


重要なメタデータ
----


### CSS: {#css}

	CSS: http://example.net/style.css

CSS スタイルシートの URL。生成される html 文書の head 要素内に、そのスタイルシートへのリンクが付け加えられる。

### Encoding: {#encoding}

	Encoding: utf-8

その文書のマルチバイトエンコーディングを表す。utf-8, euc-jp, shift_jis (shift-jis), ascii のいずれかが有効（小文字と大文字は区別しない）。
html の head 要素内に出力される Content-Type の値、および変換処理に影響する。

なお、他のヘッダーの値をマルチバイト文字列で記述する場合、 *Encoding はそれらのヘッダーよりも先に記述されていなければならない。*
そのため、このヘッダーは常に文書ファイルの最初に記述しておくことが推奨される。

省略された場合には、*エンコーディングが UTF-8 であるものとして取り扱う。*

### Title: {#title}

	Title: にんじんの美味しい調理法


その文書の名前（表題）。生成されるhtml文書の title 要素に、ここで指定した値が使われる。
省略された場合には、本文中にレベル1の見出し（h1）があればその内容を title 要素とし、なければ「no title」とする。



補助的なメタデータ
----

### Atom-Feed: {#atom-feed}
### RDF-Feed: {#rdf-feed}
### RSS-Feed: {#rss-feed}

	Atom-Feed: example.xml


ニュースフィードの URL。生成される html 文書の head 要素内に、以下のようなリンクが付け加えられ、RSS リーダーなどでそのページを簡単に登録できるようになる（オートディスカバリー）。

	<link rel="alternate" type="application/atom+xml" href="example.xml" />

どのヘッダー名を用いるかによって、生成される link 要素の type 属性値が異なる。
基本的には RSS 1.0 なら RDF-Feed を、RSS 2.0 なら RSS-Feed を、Atom (Atom Syndication Format) なら Atom-Feed を使うことが推奨される。

### Description: {#description}

	Description: 簡単にチャレンジできる、にんじんの美味しい調理法についての解説。


その文書の説明。`<meta name="description" content="～">` の内容になる。


### Keywords: {#keywords}

	Keyword: にんじん,レシピ,料理

その文書を表すキーワード。`<meta name="keywords" content="～">` の内容になる。


### Numbering: {#numbering}

	Numbering: yes

*BlueFeather 0.30以降*でのみ有効。

yes, true, on, 1のいずれかを指定すると、レベル2以降の見出し（h2～h6）に、自動で番号が振られるようになる。

使用例：

	Numbering: yes
	
	# 見出し1
	## 見出し2a
	## 見出し2b
	### 見出し3a
	### 見出し3b
	## 見出し2c
	
~

	<h1>見出し1</h1>
	  <h2>1. 見出し2a</h2>
	  <h2>2. 見出し2b</h2>
	    <h3>2.1. 見出し3a</h3>
	    <h3>2.2. 見出し3b</h3>
	  <h2>3. 見出し2c</h2>

### Numbering-Start-Level: {#numbering-start-level}

	Numbering: yes
	Numbering-Start-Level: 3

*BlueFeather 0.30以降*でのみ有効。

見出しへの番号付けを行うとき、どのレベルの見出しから番号付けの対象とするのかを指定する。
[Numbering:](#numbering)ヘッダーと同時に使われているときのみ有効。

省略した場合、レベル2以降の見出し（h2～h6）が番号付けの対象になる。