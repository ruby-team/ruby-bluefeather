# encoding: utf-8

require 'pathname'
require 'kconv'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')


describe 'Internal encoding specification:' do
	specify "utf-8 bom check" do
		src = '日本語'.kconv(Kconv::SJIS, Kconv::UTF8)
		BlueFeather::Util.utf8_bom?(src).should be_false
		bom_src = "\xef\xbb\xbf日本語"
		BlueFeather::Util.utf8_bom?(bom_src).should be_true
	end
	
	specify "'shift-jis' is available:" do
		src = '日本語'.kconv(Kconv::SJIS, Kconv::UTF8)
		BlueFeather.parse_document(src, 'shift_jis')
		BlueFeather.parse_document(src, 'shift-jis')
	end
end


describe 'Encoding of files:' do
	before(:each) do
		@html = BlueFeather.parse_document_file(Pathname.new(__FILE__).parent + 'text/' + @src_name)
		@doc = Nokogiri(@html)
	end
	
	share_as :Base do
		specify 'header parse - css' do
			@doc.should have_element('link[@rel="stylesheet"][@href="style.css"]')
		end

		specify 'header parse - encoding' do
			@doc.should have_element(%Q|meta[@http-equiv="Content-Type"][@content="text/html; charset=#{@charset}"]|)
		end
		
		specify 'header parse - title' do
			base = '日本語の題名'.kconv(@kconv_encoding, Kconv::UTF8)
			@doc.at('title').inner_html.should == base
		end
		
		specify 'content parse - p' do
			base = 'このテキストファイルは、エンコーディングテスト用のテキストファイルです。'.kconv(@kconv_encoding, Kconv::UTF8)
			@doc.at('p').inner_html.should == base
		end
		
		specify 'content parse - ul' do
			@doc.should have_elements(1, 'ul')
			
			items = @doc.search('ul li')
			%w(項目1 項目2 項目3).each_with_index do |str, i|
				items[i].inner_html.should == str.kconv(@kconv_encoding, Kconv::UTF8)
			end

		end

	end
	
	
	describe 'Default:' do
		include Base
		
		before(:all) do
			@src_name = 'encoding_sample_default.bfdoc'
			@charset = 'utf-8'
			@kconv_encoding = Kconv::UTF8
		end
	end


	describe 'UTF-8 (BOM):' do
		include Base
		
		before(:all) do
			@src_name = 'encoding_sample_utf-8.bfdoc'
			@charset = 'utf-8'
			@kconv_encoding = Kconv::UTF8
		end
	end
	
	
	describe 'UTF-8N:' do
		include Base
		
		before(:all) do
			@src_name = 'encoding_sample_utf-8n.bfdoc'
			@charset = 'utf-8'
			@kconv_encoding = Kconv::UTF8
		end
	end

	
	describe 'Shift_JIS:' do
		include Base
		
		before(:all) do
			@src_name = 'encoding_sample_shift-jis.bfdoc'
			@charset = 'shift_jis'
			@kconv_encoding = Kconv::SJIS
		end
	end
	
	describe 'EUC-JP:' do
		include Base
		
		before(:all) do
			@src_name = 'encoding_sample_euc-jp.bfdoc'
			@charset = 'euc-jp'
			@kconv_encoding = Kconv::EUC
		end
	end
end