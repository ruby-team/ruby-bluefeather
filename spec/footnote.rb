require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')

describe 'Footnote:' do
	before(:all) do
		@bf = BlueFeather::Parser.new
	end
	
	describe 'simple:' do
		before(:all) do
			@html = "<div>" + @bf.parse_text("[^testid]\n\n[^testid]: none") + "</div>"
			@doc = Nokogiri(@html)
		end
	
		specify "reference a" do
			a = @doc.at('p sup a')
			a['href'].should == '#footnote:testid'
			a.inner_html.should == '[1]'
		end
		
		specify "reference id" do
			@doc.at('p sup')['id'].should == 'footnote-ref:testid'
		end
		
		specify "footnotes" do
			div = @doc.at('div.footnotes')
			div.should have_element('hr')
			div.at('ol li')['id'].should == 'footnote:testid'
			div.at('ol li p a')['href'].should == '#footnote-ref:testid'
		end
	end
	

end