require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')


describe 'TOC:' do
	before(:each) do
		@bf = BlueFeather::Parser.new
		if @doc_src then
			@html = @bf.parse_document(@doc_src)
		else
			@html, @rs = @bf.parse_text_with_render_state(@src)
			@html = "<div>#{@html}</div>"
		end
		@doc = Nokogiri(@html)
	end

	describe 'Standard:' do
		before(:all) do
			@header_part = <<MARKDOWN
# h1 #
## h2a ##
## h2b ##
### h3a ###
### h3b ###
### h3c ###
## h2c ##
MARKDOWN
		end

	
	
		share_as :ContentOverview do
			specify 'overview' do
				@doc.should have_element('ul')
				@doc.should have_elements(1, 'h1')
				@doc.should have_elements(3, 'h2')
				@doc.should have_elements(3, 'h3')
			end
		end
	
	
		describe 'basic:' do
			include ContentOverview
			before(:all) do
				@src = "{toc}\n\n" +  @header_part
			end
			
			
			specify 'list structure' do
				toc = @doc.at('ul')
				toc['class'] = 'toc'
				
				@doc.should have_elements(3, 'ul.toc > li')
				@doc.should have_elements(1, 'ul.toc > li > ul')
				@doc.should have_elements(3, 'ul.toc > li > ul > li')
			end
			
			specify 'list content' do
				toc = @doc.at('ul')
				toc['class'] = 'toc'
				
				items1 = @doc.search('ul.toc > li')
				items1[0].inner_text.should == 'h2a'
				items1[2].inner_text.should == 'h2c'
	
				items2 = @doc.search('ul.toc > li > ul > li')
				items2[0].inner_text.should == 'h3a'
				items2[1].inner_text.should == 'h3b'
				items2[2].inner_text.should == 'h3c'
			end
	
		end
		
		
		describe 'range:' do
			include ContentOverview
			before(:all) do
				@src = "{toc:h1..h2}\n\n" +  @header_part
			end
			
			specify 'list structure' do
				toc = @doc.at('ul')
				toc['class'] = 'toc'
				
				@doc.should have_elements(1, 'ul.toc > li')
				@doc.should have_elements(1, 'ul.toc > li > ul')
				@doc.should have_elements(3, 'ul.toc > li > ul > li')
			end
			
			specify 'list content' do
				toc = @doc.at('ul')
				toc['class'] = 'toc'
				
				items2 = @doc.search('ul.toc > li > ul > li')
				items2[0].inner_text.should == 'h2a'
				items2[1].inner_text.should == 'h2b'
				items2[2].inner_text.should == 'h2c'
			end
	
		end
		
		
		describe 'numbering:' do
			before(:all) do
				@doc_src = "Numbering: yes\n\n{toc}\n" + @header_part
			end
			
			specify 'list content' do
				toc = @doc.at('ul')
				toc['class'] = 'toc'
				
				items1 = @doc.search('ul.toc > li')
				items1[0].inner_text.should == '1. h2a'
				items1[2].inner_text.should == '3. h2c'
				
				items2 = @doc.search('ul.toc > li > ul > li')
				items2[0].inner_text.should == '2.1. h3a'
				items2[1].inner_text.should == '2.2. h3b'
				items2[2].inner_text.should == '2.3. h3c'

			end
		end

		

	end
	
	describe 'Illegal:' do
		describe 'Standard:' do
			before(:all) do
				@src = <<MARKDOWN
{toc}
	
### h3
MARKDOWN
			end
			
			specify '1 warning' do
				@rs.warnings.size.should == 1
			end
		end
	end
	
	


end