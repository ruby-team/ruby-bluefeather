require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')


describe 'Escape Characters:' do
	before(:each) do
		@bf = BlueFeather::Parser.new
	end
	
	'\\`*_{}[]()#.!|:~'.each_char do |c|
		specify "'#{c}'" do
			@bf.parse_text("\\#{c}").should == "<p>#{c}</p>"
		end
		
		specify "'#{c}' (in code block)" do
			@bf.parse_text("    \\#{c}").should == "<pre><code>\\#{c}\n</code></pre>"
		end
	end
end

describe 'Backquote Escape:' do
	specify do
		BlueFeather.parse_text('`<http://example.net/>`').should == "<p><code>&lt;http://example.net/&gt;</code></p>"
	end
end