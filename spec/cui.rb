require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')

require 'stringio'
require 'bluefeather/cui'

include BlueFeather

describe 'CUI:' do
	def clear_io
		@stdout.string = ''
		@stderr.string = ''
	end

	before(:each) do
		@stdout = StringIO.new
		@stderr = StringIO.new
		@stdin = StringIO.new
		@cui = CUI.new(@stdout, @stderr, @stdin)
	end
	
	specify 'no argument' do
		@cui.run([])
		@cui.should_not wrote_to_stdout
		@cui.should wrote_to_stderr("ERROR: please text file paths, patterns, or '-' (stdin-mode).\nEx) bluefeather *.bfdoc\n")
	end

	
	specify '-h / --help' do
		@cui.run(%w(-h))
		@cui.should wrote_to_stdout(CUI::HELP)
		@cui.should_not wrote_to_stderr
		
		clear_io

		@cui.run(%w(-h unknown-file))
		@cui.should wrote_to_stdout(CUI::HELP)
		@cui.should_not wrote_to_stderr
	end
	
	specify '--version' do
		@cui.run(%w(--version unknown-file))
		@cui.should wrote_to_stdout("bluefeather #{BlueFeather::VERSION_LABEL}\n")
		@cui.should_not wrote_to_stderr
	end
	
	describe 'Encoding Option:' do
		specify "-e == --encoding" do
			Proc.new{@cui.run(['-e', 's'])}.should_not raise_error
		end
		
		specify "name is required" do
			Proc.new{@cui.run(['--encoding'])}.should raise_error(OptionParser::MissingArgument)
		end

		valids = %w(s shift-jis shift_jis sjis u utf-8 UTF-8 e euc-jp a ascii)
		sample_src = (Pathname.new(__FILE__).parent + 'text/encoding_sample_ascii.bfdoc').to_s
		valids.each do |name|
			specify "'#{name}' is valid" do
				@cui.run(['--encoding', name, '-']).should be_true
			end
		end
		
		invalids = %w(euc_jp none jis)
		invalids.each do |name|
			specify "'#{name}' is invalid" do
				@cui.run(['--encoding', name, '-']).should be_false
			end
		end
		
	end


end