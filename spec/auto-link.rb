require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')


describe 'Auto Link:' do
	before(:each) do
		@html = BlueFeather.parse(@src)
		@doc = Nokogiri(@html)
	end

	describe 'Valid URL:' do
		
		data = [
			'http://www.example.com/',
			'https://www.example.com/',
			'skype:example-id',
			'mailto:nobody@example.net',
		]
	
		data.each do |url|
			src = "<#{url}>"
			describe src do
				before(:all) do
					@src = src
				end
				
				specify 'p > a' do
					@doc.should have_elements(1, 'p')
					@doc.should have_elements(1, 'p a')
				end
			
				specify 'text' do
					@doc.at('a').inner_text.should == url
				end
				
				specify 'href' do
					@doc.at('a')['href'].should == url
				end

			end
		end
	end
	
	describe 'Invalid URL:' do
		
		data = [
			'http//www.example.com/',
		]
	
		data.each do |url|
			src = "<#{url}>"
			describe src do
				before(:all) do
					@src = src
				end
				
				specify 'no anchor' do
					@doc.should have_element('p')
					@doc.should_not have_element('p a')
				end

			end
		end


	end



	describe 'Valid Address:' do
		
		data = [
			'nobody.user@example.com',
			'nobody.user@example.com',
			'.nobody..user.@example.com',
		]
	
		data.each do |address|
			src = "<#{address}>"
			describe src do
				before(:all) do
					@src = src
				end
				
				specify 'overview' do
					@doc.should have_elements(1, 'p')
					@doc.should have_elements(1, 'p a')
				end
			

			end
		end

	end


	


end