require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')


describe 'Specified Header ID:' do
	before(:each) do
		@bf = BlueFeather::Parser.new
		@html = @bf.parse_text(@src)
		@doc = Nokogiri(@html)
	end


	describe 'basic:' do
		before(:all) do
			@src = '## h2 ## {#test-id}'
		end
		
		specify 'correct id' do
			@doc.should have_element('h2#test-id')
		end

	end

	
	describe 'use span transform format:' do
		before(:all) do
			@src = '## h2 ## {#the_test_id}'
		end
		
		specify 'correct id' do
			@doc.should have_element('h2#the_test_id')
		end

	end
end

describe 'Auto Generated Header ID:' do
	before(:each) do
		@bf = BlueFeather::Parser.new
		@rs = BlueFeather::Parser::RenderState.new
		@rs.header_id_type = @header_id_type if @header_id_type
		@html, @rs = @bf.parse_text_with_render_state('# h1/header 1 #', @rs)
		@doc = Nokogiri("<div>" + @html + "</div>")
	end


	describe 'default (=md5):' do
		specify 'pattern matched' do
			@doc.at('h1')['id'].should =~ /^bfheader-[0-9a-f]{32}$/
			@rs.warnings.size.should == 0
		end
	end
	
	describe 'md5:' do
		before(:all) do
			@header_id_type = 'md5'
		end
	
		specify 'pattern matched' do
			@doc.at('h1')['id'].should =~ /^bfheader-[0-9a-f]{32}$/
			@rs.warnings.size.should == 0
		end
	end
	
	describe 'escape:' do
		before(:all) do
			@header_id_type = 'escape'
		end
	
		specify 'pattern matched' do
			@doc.at('h1')['id'].should == 'h1.2Fheader_1'
			@rs.warnings.size.should == 0
		end
	end


	describe 'illegel type:' do
		before(:all) do
			@header_id_type = '*illegal*'
		end
	
		specify 'pattern matched' do
			@doc.at('h1')['id'].should =~ /^bfheader-[0-9a-f]{32}$/
			@rs.warnings.size.should == 1
		end
	end
end

describe "Document Header (Header-ID-Type:)" do
	%w(md5 MD5).each do |value|
		specify value do
			doc = BlueFeather::Document.new({'Header-ID-Type' => value})
			doc.header_id_type.should == BlueFeather::HeaderIDType::MD5
		end
	end
	
	%w(escape Escape ESCAPE).each do |value|
		specify value do
			doc = BlueFeather::Document.new({'Header-ID-Type' => value})
			doc.header_id_type.should == BlueFeather::HeaderIDType::ESCAPE
		end
	end

end

describe 'Anchor Briefing Link:' do
	specify 'equal' do
		BlueFeather.parse_text('[FooBar](#)').should == BlueFeather.parse_text('[FooBar](#FooBar)')
	end
end