require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')

if RUBY_PLATFORM =~ /win|mingw/ then

	describe 'DOS Pathname:' do
	
		before do
			@prefix = File.expand_path(File.dirname(__FILE__))
			@unix_path = "#{@prefix}/text/encoding_sample_default.bfdoc"
			@dos_path = "#{@prefix.gsub('/', '\\')}\\text\\encoding_sample_default.bfdoc"
			@cmd_path = Pathname.new(__FILE__).parent.parent + 'bin/bluefeather'
			@lib_dir = Pathname.new(__FILE__).parent.parent + 'lib'
		end
		
		specify 'command run' do
			File.exist?(@unix_path).should be_true
			File.exist?(@dos_path).should be_true
			
			system("ruby -I #{@lib_dir} #{@cmd_path} --quiet #{@unix_path}").should be_true
			system("ruby -I #{@lib_dir} #{@cmd_path} --quiet #{@dos_path}").should be_true
		end
		
		after do
			dest = @unix_path.sub(/.bfdoc$/, '.html')
			File.unlink(dest) if File.exist?(dest)
		end
	end
else
	puts "spec/win32.rb skipped."
end