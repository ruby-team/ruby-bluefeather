require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')


describe 'Simple Table:' do
	before(:each) do
		@bf = BlueFeather::Parser.new
			
		@html = @bf.parse_text(@src)
		@doc = Nokogiri(@html)
	end
	
	share_as :StandardTable do
		specify "table overview" do
			@doc.should have_elements(1, 'table')
		end
		
		specify "size 3x3" do
			@doc.should have_elements(3, 'tr')
			@doc.should have_elements(1, 'thead tr')
			@doc.should have_elements(2, 'tbody tr')
			@doc.should have_elements(3, 'thead tr th')
			@doc.should have_elements(6, 'tbody tr td')
		end
		
		specify "alignment" do
			@doc.search('tr').each do |tr|
				cells = tr.search('th, td')
				cells[0]['style'].should be_nil
				cells[1]['style'].should == 'text-align: right'
				cells[2]['style'].should == 'text-align: center'
			end
		end
		
		specify "purity" do
			@doc.should_not have_element('p table')
		end
	end


	describe 'no-doubleline' do
		include StandardTable
	
		before(:all) do
			@src = "|h1|h2|h3\n|-|-:|:-:\n|d11|d21|d31\n|d12|d22|d32"
		end
	end

	
	
	describe 'doubleline: ' do
		include StandardTable

		before(:all) do
			@src = "|h1|h2|h3|\n|-|-:|:-:|\n|d11|d21|d31|\n|d12|d22|d32|"
		end
	end
	
	describe 'inner space:' do
		include StandardTable
	
		before(:all) do
			@src = "| h1 | h2 | h3 \n| - | -: | :-: \n| d11 | d21 | d31\n| d12 | d22 | d32"
		end
	end

	describe 'left space:' do
		include StandardTable
	
		before(:all) do
			@src = " | h1 | h2 | h3 \n | - | -: | :-: \n | d11 | d21 | d31\n | d12 | d22 | d32"
		end
	end


	describe 'in definition list:' do
		include StandardTable
	
		before(:all) do
			@src = <<MARKDOWN
Apple

: Apple content

	| h1 | h2 | h3 
	| - | -: | :-: 
	| d11 | d21 | d31
	| d12 | d22 | d32
MARKDOWN
		end
	end


end