require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')

describe 'Normal Code Block:' do
	before(:each) do
		@bf = BlueFeather::Parser.new
		@html = @bf.parse_text(@src)
		@doc = Nokogiri(@html)
	end


	describe '> ol:' do
		before(:all) do
			@src = "    1. item-1\n    2. item-2\n    3. item-3\n"
		end
		
		specify 'basic' do
			@doc.should have_elements(1, 'pre code')
			@doc.should_not have_element('ol')
		end
	end
end

describe 'Fenced Code Block:' do
	before(:each) do
		@bf = BlueFeather::Parser.new
		@html = @bf.parse_text(@src)
		@doc = Nokogiri(@html)
	end


	describe 'Basic:' do
		share_as :Examples do
			specify 'overview' do
				@doc.should have_elements(1, 'pre code')
				@doc.should_not have_element('p')
			end
			
			specify 'body' do
				@doc.at('pre code').inner_text.should == "Fenced Code\nBlock\n"
			end
		end
		
		share_as :NegativeExamples do
			specify 'overview' do
				@doc.should_not have_elements(1, 'pre code')
				@doc.should have_elements(1, 'p')
			end
		end

	
		describe 'Standard:' do
			include Examples
			before(:all) do
				@src = "~~~\nFenced Code\nBlock\n~~~"
			end
		end
		

		
		describe 'Many Symbols:' do
			include Examples
			before(:all) do
				@src = "~~~~~~\nFenced Code\nBlock\n~~~~~~"
			end
		end
		
		describe 'Original Code Block:' do
			include Examples
			before(:all) do
				@src = "    Fenced Code\n    Block"
			end
		end
		
		describe 'Bad (wrong number of symbols):' do
			include NegativeExamples
			before(:all) do
				@src = "~~~~~~\nFenced Code\nBlock\n~~~"
			end
		end

		describe 'Bad (symbols is not on line-head.):' do
			include NegativeExamples
			before(:all) do
				@src = " ~~~\nFenced Code\nBlock\n ~~~"
			end
		end

	end


	describe '0.20 Bug:' do
		describe 'Last-line Header:' do
			before(:all) do
				@src = "~~~\nFenced Code\nBlock\n# Last-line header\n~~~"
			end
			
			specify 'overview' do
				@doc.should have_elements(1, 'pre code')
				@doc.should_not have_element('p')
				@doc.should_not have_element('h1')
			end
				
			specify 'body' do
				@doc.at('pre code').inner_text.should == "Fenced Code\nBlock\n# Last-line header\n"
			end
		end
		
		describe 'Definition List:' do
			before(:all) do
				@src = "~~~\nFenced Code\nBlock\n: dd\n~~~"
			end
			
			specify 'overview' do
				@doc.should have_elements(1, 'pre code')
				@doc.should_not have_element('p')
				@doc.should_not have_element('dl')
			end
				
			specify 'body' do
				@doc.at('pre code').inner_text.should == "Fenced Code\nBlock\n: dd\n"
			end
		end

		describe 'Code block from second line:' do
			before(:all) do
				@src = "\n    a\n    b"
			end
			
			specify 'overview' do
				@doc.should have_elements(1, 'pre code')
				@doc.should_not have_element('p')
			end
				
			specify 'body' do
				@doc.at('pre code').inner_text.should == "a\nb\n"
			end
		end
	end
	
	

end

describe "0.30 code block bug in list item:" do
	before(:each) do
		@bf = BlueFeather::Parser.new
		@html = @bf.parse_text(@src)
		@doc = Nokogiri(@html)
	end
	
	before(:all) do
		@src = <<MARKDOWN
* example list

	~~~
	code line a
	
	code line b
	~~~
MARKDOWN
	end
	
	specify 'overview' do
		@doc.should have_elements(1, 'ul li pre code')
	end
			
	specify 'body' do
		@doc.at('pre code').inner_text.should == "code line a\n\ncode line b\n"
	end

end

describe "0.31 code block bug in list item:" do
	before(:each) do
		@bf = BlueFeather::Parser.new
		@html = @bf.parse_text(@src)
		@doc = Nokogiri(@html)
	end
	
	describe 'simple:' do
		before(:all) do
			@src = <<MARKDOWN
~~~
* example list in code block

    ~~~
    code
    ~~~
~~~
MARKDOWN
		end
		
		specify 'overview' do
			@doc.should have_elements(1, 'pre code')
			@doc.should_not have_element('pre code pre')
		end
				
		specify 'body' do
			@doc.at('pre code').inner_text.should == "* example list in code block\n\n    ~~~\n    code\n    ~~~\n"
		end

	end
	
	describe 'complex:' do
	
		before(:all) do
			@src = <<MARKDOWN
* example list

	~~~
	* example list in code block
	
			code
	~~~
MARKDOWN
		end
		
		specify 'overview' do
			pending 'not fixed'
			@doc.should have_elements(1, 'ul li pre code')
			@doc.should_not have_element('pre code pre')
		end
				
		specify 'body' do
			pending 'not fixed'
			@doc.at('pre code').inner_text.should == "* example list in code block\n\n    ~~~\n    code\n    ~~~\n"
		end
	end
end

describe "0.31 code block bug in list item:" do
	before(:each) do
		@bf = BlueFeather::Parser.new
		@html = @bf.parse_text(@src)
		@doc = Nokogiri(@html)
	end
	
	describe 'simple:' do
		before(:all) do
			@src = <<MARKDOWN
	f<a < b>(a)
MARKDOWN
		end
		
		specify 'overview' do
			@doc.should have_elements(1, 'pre code')
			@doc.should_not have_element('pre code pre')
		end

	end
	
end