require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')


describe 'Definition List:' do
	before(:each) do
		@bf = BlueFeather::Parser.new
		@html = @bf.parse_text(@src)
		@doc = Nokogiri(@html)
	end
	
	describe 'Basic:' do
		describe 'most standard:'do
			before(:all) do
				@src = ("dt1\n:dd1\n\ndt2\n:dd2-\ndouble-line")
			end
			
			specify 'overview' do
				@doc.should have_elements(1, 'dl')
				@doc.should have_elements(2, 'dl dt')
				@doc.should have_elements(2, 'dl dd')
			end
			
			specify 'text' do
				@doc.search('dl dt')[0].inner_text.should == "dt1"
				@doc.search('dl dt')[1].inner_text.should == "dt2"
				@doc.search('dl dd')[0].inner_text.should == "dd1"
				@doc.search('dl dd')[1].inner_text.should == "dd2-\ndouble-line"
			end

		end
		
		describe 'double dt:'do
			before(:all) do
				@src = ("dt1\ndt2\n:dd")
			end
			
			specify 'overview' do
				@doc.should have_elements(1, 'dl')
				@doc.should have_elements(2, 'dl dt')
				@doc.should have_elements(1, 'dl dd')
			end
			
			specify 'text' do
				@doc.search('dl dt')[0].inner_text.should == 'dt1'
				@doc.search('dl dt')[1].inner_text.should == 'dt2'
				@doc.at('dl dd').inner_text.should == "dd"
			end
		end
		
		describe 'double dd:'do
			before(:all) do
				@src = ("dt\n:dd1\n:dd2")
			end
			
			specify 'overview' do
				@doc.should have_elements(1, 'dl')
				@doc.should have_elements(1, 'dl dt')
				@doc.should have_elements(2, 'dl dd')
			end
			
			specify 'text' do
				@doc.search('dl dd')[0].inner_text.should == "dd1"
				@doc.search('dl dd')[1].inner_text.should == "dd2"
				@doc.at('dl dt').inner_text.should == 'dt'
			end
		end


	end


	describe '> code-block > ol:' do
		before(:all) do
			@src = <<-SRC
title

:

		1. olitem-1
		2. olitem-2
		3. olitem-3
        
			SRC
		end
		
		specify 'overview' do
			@doc.should have_elements(1, 'dl dt')
			@doc.should have_elements(1, 'dl dd')
		end
		
		specify 'dt text' do
			@doc.at('dt').inner_text.should == 'title'
		end
		
		specify 'ol in code-block' do
			@doc.should have_element('dd pre code')
			
			code = @doc / 'dd pre code'
			code.inner_text.should =~ /\A1.+\n2.+\n3.+\n\z/
		end

	end
	
	describe '> code-block > ol(with paragraphs):' do
		before(:all) do
			@src = <<-SRC
title

:paragraph1

		1. olitem-1
		2. olitem-2
		3. olitem-3
		
	paragraph2

			SRC
		end
		
		specify 'overview' do
			@doc.should have_elements(1, 'dl dt')
			@doc.should have_elements(1, 'dl dd')
			@doc.should have_elements(2, 'dl dd p')
		end
		
		specify 'dt text' do
			@doc.at('dt').inner_text.should == 'title'
		end
		
		specify 'ol in code-block' do
			@doc.should have_element('dd pre code')
			
			code = @doc / 'dd pre code'
			code.inner_text.should =~ /\A1.+\n2.+\n3.+\n\z/
		end
		
		specify 'paragraphs' do
			paragraphs = @doc / 'dl dd p'
			paragraphs[0].inner_text.should == 'paragraph1'
			paragraphs[1].inner_text.should == 'paragraph2'
		end

	end
	

	describe 'blockquote > code-block > ol and paragraphs:' do
		before(:all) do
			@src = <<-SRC
>title
>
>:paragraph1
>
>         1. olitem-1
>         2. olitem-2
>         3. olitem-3
>
>     paragraph2

			SRC
		end
		
		specify 'overview' do
			@doc.should have_elements(1, 'blockquote dl dt')
			@doc.should have_elements(1, 'blockquote dl dd')
			@doc.should have_elements(2, 'blockquote dl dd p')
		end
		
		specify 'dt text' do
			@doc.at('blockquote dt').inner_text.should == 'title'
		end
		
		specify 'paragraphs' do
			paragraphs = @doc / 'blockquote dl dd p'
			paragraphs[0].inner_text.should == 'paragraph1'
			paragraphs[1].inner_text.should == 'paragraph2'
		end

	end


end