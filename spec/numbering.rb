require 'pathname'
require(Pathname.new(__FILE__).parent + 'lib/common.rb')


describe 'Numbering:' do
	before(:each) do
		@src = <<MARKDOWN
Numbering: #{@numbering}
Numbering-Start-Level: #{@start_level}

# h1
## h2a
## h2b
### h3a
### h3b
## h2c
MARKDOWN

		@html = BlueFeather.parse_document(@src)
		@doc = Nokogiri(@html)
	end
	
	
	describe "yes:" do
		before(:all) do
			@numbering, @start_level = 'yes  ', ''
		end

		specify "numbering" do
			@doc.at('h1').inner_text.should == 'h1'
			
			elems = @doc.search('h2')
			elems.map{|x| x.inner_text}.should == ['1. h2a', '2. h2b', '3. h2c']
			
			elems = @doc.search('h3')
			elems.map{|x| x.inner_text}.should == ['2.1. h3a', '2.2. h3b']
		end

	end
	
	describe "yes (start=3):" do
		before(:all) do
			@numbering, @start_level = 'yes  ', '3 '
		end

		specify "numbering" do
			@doc.at('h1').inner_text.should == 'h1'
			
			elems = @doc.search('h2')
			elems.map{|x| x.inner_text}.should == ['h2a', 'h2b', 'h2c']
			
			elems = @doc.search('h3')
			elems.map{|x| x.inner_text}.should == ['1. h3a', '2. h3b']
		end

	end

end