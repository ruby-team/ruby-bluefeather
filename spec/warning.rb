require 'bluefeather'

describe 'Warning:' do
	before(:all) do
		@bf = BlueFeather::Parser.new
	end
	
	specify "header level" do
		html, rs = @bf.parse_text_with_render_state('###### h6')
		rs.warnings.size.should == 0
		
		html, rs = @bf.parse_text_with_render_state('####### h7')
		rs.warnings.size.should == 1
	end

	
	specify "header id" do
		html, rs = @bf.parse_text_with_render_state('# header # {#legal}')
		rs.warnings.size.should == 0
		
		html, rs = @bf.parse_text_with_render_state('# header # {#99illegal}')
		rs.warnings.size.should == 1

		html, rs = @bf.parse_text_with_render_state('# header # {#ille?gal}')
		rs.warnings.size.should == 1
	end
	
	specify "link id reference" do
		html, rs = @bf.parse_text_with_render_state("[markdown][]\n\n[markdown]: -")
		rs.warnings.size.should == 0

		html, rs = @bf.parse_text_with_render_state("[unknown][]\n\n[markdown]: -")
		rs.warnings.size.should == 1
	end


	
	specify "footnote id" do
		html, rs = @bf.parse_text_with_render_state('[^markdown]: -')
		rs.warnings.size.should == 0

		html, rs = @bf.parse_text_with_render_state('[^mark?down]: -')
		rs.warnings.size.should == 1
	end

	specify "footnote reference" do
		html, rs = @bf.parse_text_with_render_state("[^1]\n\n[^1]: -")
		rs.warnings.size.should == 0
		
		html, rs = @bf.parse_text_with_render_state("[^unknown]")
		rs.warnings.size.should == 1
	end
	
	specify "toc parameter" do
		html, rs = @bf.parse_text_with_render_state("{toc:h2..}")
		rs.warnings.size.should == 0
		
		html, rs = @bf.parse_text_with_render_state("{toc:h21..}")
		rs.warnings.size.should == 1
	end
end