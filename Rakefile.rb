# encoding: us-ascii
#-----------------------------------------------------------
# BlueFeather rakefile
#
# Requirement:
#   Ruby (1.8.1 later)
#   Rake - <http://rake.rubyforge.org/> or `gem install rake` with RubyGems
#   Exerb - <http://exerb.sourceforge.jp/>
#   Info-Zip - If you use Windows, you can get it from GnuWin32
#                                  <http://gnuwin32.sourceforge.net/>
#   Rspec - `gem install rspec` with RubyGems
#-----------------------------------------------------------

require 'rake/testtask'
require 'rubygems/package_task'
require 'rake/clean'


# Adaptable to both RSpec 1 and 2
rspec_version = nil
begin
	# for RSpec 2
	require 'rspec/core/rake_task'
	rspec_version = 2
rescue LoadError
	begin
		# for RSpec 1
		require 'spec/rake/spectask'
		rspec_version = 1
	rescue LoadError
		puts "RSpec is not available."
	end
end


$KCODE = 'u' unless defined?(Encoding)
$LOAD_PATH.unshift './lib'
require 'bluefeather'

ZIP_NAME = "pkg/bluefeather-#{BlueFeather::VERSION}.zip"

CLOBBER.include 'sample/*.html'
CLOBBER.include 'doc/**/*.html'

# setup.rb
CLEAN.include('InstalledFiles', '.config', 'bin/bluefeather.bat')

PACKAGED = FileList.new
PACKAGED.include('readme*.txt')
PACKAGED.include('setup.rb')
PACKAGED.include('Rakefile.rb')
PACKAGED.include('bin/**/*')
PACKAGED.include('lib/bluefeather.rb')
PACKAGED.include('lib/bluefeather/*.rb')
PACKAGED.include('license/**/*')
PACKAGED.include('spec/**/*')
PACKAGED.include('original-tests/**/*')
PACKAGED.include('doc/**/*.*')
PACKAGED.exclude('doc/**/format-extension-spec.*')



task :default => :package

desc 'Package to zip.'
task :package => [:doc, ZIP_NAME]

desc 'Clobber and package.'
task :repackage => [:clobber, :package]

file ZIP_NAME => PACKAGED do |task|
	mkpath 'pkg'
	src_list = task.prerequisites.map{|x| %Q|"#{x}"|}.join(' ')
	sh "zip -q #{task.name} #{src_list}"
	puts "#{task.name} maked. (#{File.size(task.name) / 1024} KB)"
end




DOCS = []
Dir.glob('doc/**/*.bfdoc').each do |src|
	dest = src.sub(/\.bfdoc$/, '.html')
	DOCS << dest

	file dest => src do
		html = BlueFeather.parse_document_file(src)
		open(dest, 'w'){|f| f.write(html)}
		puts "#{src} => #{dest}"
	end
end

desc 'Convert documents, bfdoc -> html'
task :doc => DOCS

task :sample => 'sample/sample.html'

file 'sample/sample.html' => 'sample/sample.bfdoc' do |task|
	html = BlueFeather.parse_document_file(task.prerequisites.first)
	open(task.name, 'w'){|f| f.write(html)}
	puts "#{task.prerequisites.first} => #{task.name}"
end




GEM_PACKAGED = PACKAGED.dup
GEM_PACKAGED.exclude('setup.rb')

spec = Gem::Specification.new do |s|
	s.platform = Gem::Platform::RUBY
	s.summary = "Extend Markdown Converter"
	s.author = 'Dice'
	s.email = 'tetradice@gmail.com'
	s.homepage = 'http://ruby.morphball.net/bluefeather/'
	s.name = 'bluefeather'
	s.rubyforge_project = 'bluefeather'
	s.has_rdoc = false
	s.required_ruby_version = '>= 1.8.1'
	s.version = BlueFeather::VERSION
	s.files = GEM_PACKAGED
	s.executables = ['bluefeather']
	s.description = <<EOF
 BlueFeather is software for converting text written by extended Markdown to
html. It is pair of command-line tool and pure Ruby library.
EOF
end

Gem::PackageTask.new(spec) do |pkg|
end






desc 'Run tests of original BlueCloth.'
Rake::TestTask.new('original-test') do |tasklib|
	tasklib.test_files = FileList['original-tests/*.tests.rb']
end


if rspec_version then
	case rspec_version
	when 1
		task_cls = Spec::Rake::SpecTask
		rspec_opts_setting_proc = proc do |task|
			task.spec_opts << '-fs'
		end
	when 2
		task_cls = RSpec::Core::RakeTask
		rspec_opts_setting_proc = proc do |task|
			task.rspec_opts = ['-fs']
		end
	else
		raise 'unknown rspec version.'
	end
	
	# Proc for initializing each spec-tasks by common parameters
	setting = proc do |st|
		st.ruby_opts = %w(-Ku)
	end
	
	
	desc "Verify all spec files."
	task_cls.new do |st|
		setting.call(st)
		st.pattern = 'spec/*.rb'
	end
	
	desc "Verify all spec files with specdocs."
	task_cls.new(:specd) do |st|
		setting.call(st)
		rspec_opts_setting_proc.call(st)
		st.pattern = 'spec/*.rb'
	end
	
	
	
	namespace :spec do
		Dir.glob('spec/*.rb') do |path|
			desc "Verify '#{path}'"
			task_cls.new(File.basename(path, '.rb')) do |st|
				setting.call(st)
				st.pattern = path
			end
		end
	end
	
	namespace :specd do
		Dir.glob('spec/*.rb') do |path|
			desc "Verify '#{path}' with specdocs."
			task_cls.new(File.basename(path, '.rb')) do |st|
				setting.call(st)
				rspec_opts_setting_proc.call(st)
				st.pattern = path
			end
		end
	end
	
	desc '= spec + original-test'
	task 'test-all' => [:spec, 'original-test']
end
